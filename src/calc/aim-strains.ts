import { double, int } from "../utils";
import { Variables } from "../vars";
import { Vector2 } from "../hit-objects/vector2";
import { AimPointType, IAimPoint } from "../prep/gather-aim-points";

export function calculateAimStrains(beatmap: IThingWithAimPoints): number[] {
    let aimStrains: number[] = [];
    let aimPoints = beatmap.aimPoints;
    
    //  for the first aim point
    aimStrains.push(0);
    
    let oldstrain: double = 0;
    let len = aimPoints.length;
	for (let i: int = 1; i < len; i++)
	{
		let strain: double = 0;
        let distance = Vector2.distance(aimPoints[i].pos, aimPoints[i - 1].pos);
        let weightedDistance: double = GetWeightedAimDistance(distance);
        let interval: int = Math.floor(aimPoints[i].time - aimPoints[i - 1].time);
        let time: double = GetWeightedAimTime(interval);
        let angleBonus: double = 1;
        if(i > 1)
            angleBonus = 1 + (Variables.Agility.AngleMult * beatmap.angleBonuses[i - 2]);

        if (time > 0)
            strain = weightedDistance / time * angleBonus;
        else
        {
            console.log(`${beatmap.name} Agility strain calc: time ${time}`);
            continue;
        }

        //  decrease weight for a slider/slider end
        //  NOTSURE: so basically type.Slider || type.SliderEnd?
        if (aimPoints[i].type === AimPointType.SliderEnd || aimPoints[i - 1].type === AimPointType.SliderEnd)
            strain *= Variables.Agility.SliderStrainDecay;

        oldstrain -= Variables.Agility.StrainDecay * interval;
        if (oldstrain < 0)
            oldstrain = 0;

        strain += oldstrain;

		aimStrains.push(strain);
		oldstrain = strain;
    }

    return aimStrains;
}

function GetWeightedAimDistance(distance: double): double {
	let distanceBonus: double = Math.pow(1 + (distance * Variables.Agility.DistMult), Variables.Agility.DistPow);
	distanceBonus /= Variables.Agility.DistDivisor;
	return distance * distanceBonus;
}

function GetWeightedAimTime(time: double): double {
	let timeBonus: double = Math.pow(time * Variables.Agility.TimeMult, Variables.Agility.TimePow);
	return time * timeBonus;
}

export interface IThingWithAimPoints {
    /**
     * used for debugging purposes
     */
    name: string;

    aimPoints: IAimPoint[];

    /**
     * used for reading
     */
    angleBonuses: double[];
}