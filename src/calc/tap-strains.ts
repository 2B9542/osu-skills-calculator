import { double, int } from "../utils";
import { Variables } from "../vars";

export function calculateTapStrains(beatmap: IS): double[] {
	let c: int = 0;
	let oldbonus: double;
    let strain: double = 0;
    let tapStrains: double[] = [];
    
    beatmap.pressIntervals.forEach(interval => {
        if(c === 0) {
            if (interval >= Variables.Stamina.LargestInterval)
                strain = 0; // maybe delete this
            else
                strain = Variables.Stamina.Scale / Math.pow(interval, Math.pow(interval, Variables.Stamina.Pow) * Variables.Stamina.Mult);
        }
        else {
            if (interval >= Variables.Stamina.LargestInterval)
				strain *= Variables.Stamina.DecayMax;
			else
			{
				if (interval <= 1)
					return;

                strain = Variables.Stamina.Scale / Math.pow(interval, Math.pow(interval, Variables.Stamina.Pow) * Variables.Stamina.Mult);
				//cout << "Strain for current: " << strain << endl;
				strain += oldbonus * Variables.Stamina.Decay;
				//cout << "New strain " << strain << ", bonus: " << oldbonus * SEQUENCE_MULTIPLIER << endl;
            }
        }

        tapStrains.push(strain);
        oldbonus = strain;
		c++;
    });

    return tapStrains;
}

export interface IS {
    pressIntervals: int[];
}