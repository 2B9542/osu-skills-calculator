import { IBeatmap, } from "../../beatmap";
import { max_element, } from "../../utils";
import { Variables } from "../../vars";

export function calculateStamina(beatmap: IBeatmap) {
    let max = max_element(beatmap.tapStrains);
    return Variables.Stamina.TotalMult * Math.pow(max, Variables.Stamina.TotalPow);
}
