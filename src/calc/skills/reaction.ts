import { IHitObject } from "../../hit-objects/base";
import ModType from "../../mod-type";
import { IBeatmap, ITiming, } from "../../beatmap";
import { Variables } from "../../vars";
import { ActualPI, AR2ms, BTWN, CS2px, double, GetAngle, int, Pair } from "../../utils";
import { Vector2 } from "../../hit-objects/vector2";

export function calculateReaction(beatmap: IBeatmap): double {
    let max = 0;
    let avg = 0;
    let weight = Variables.Reaction.AvgWeighting;
    let hidden = beatmap.hasMod(ModType.HD);

    beatmap.targetPoints.forEach(tick => {
        let val: number = getReactionSkillAt(
            beatmap.targetPoints,
            tick,
            beatmap.hitObjects,
            beatmap.cs,
            beatmap.ar,
            hidden
        );

        if (val > max)			max = val;
        if (val > max / 2.0)	avg = weight * val + (1 - weight) * avg;
    });

    return (max + avg) / 2.0;
}

function getReactionSkillAt(
    targetpoints: ITiming[],
    targetpoint: ITiming,
    hitobjects: IHitObject[],
    CS: number,
    AR: number,
    hidden: boolean
): number 
{
    let timeToReact = 0.0;
    //  players can react once the note is 10% faded in
	let FadeInReactReq = Variables.Reaction.FadeinPercent;
	let index = findTimingAt(targetpoints, targetpoint.time);

	if (index >= targetpoints.length - 2)
	{
		timeToReact = AR2ms(AR);
	}
	else if (index < 3)
	{
        let visibilityTimes: Pair<number, number> = 
            hitobjects[0].getVisibilityTimes({
                AR,
                hidden,
                opacityStart: FadeInReactReq,
                opacityEnd: 1.0,
            });
		timeToReact = hitobjects[0].time - visibilityTimes.first;
	}
	else
	{
		let t1: ITiming = targetpoints[index];
		let t2: ITiming = targetpoints[index + 1];
		let t3: ITiming = targetpoints[index + 2];

		let timeSinceStart = 0;

		if (targetpoint.press == true)
			timeSinceStart = Math.abs(targetpoint.time - hitobjects[targetpoint.key].time);  // Time since started holding slider

        let visibilityTimes: Pair<number, number> = 
            hitobjects[0].getVisibilityTimes({
                AR,
                hidden,
                opacityStart: FadeInReactReq,
                opacityEnd: 1.0,
            });
		let actualARTime = (hitobjects[0].time - visibilityTimes.first) + timeSinceStart;

		let result: number = pattern2Reaction(t1, t2, t3, actualARTime, CS2px(CS));
		timeToReact = Math.sqrt(timeToReact * timeToReact + result * result);
	}

    //  to fit it on scale compared to other skills (v2)
	return Variables.Reaction.VerScale * Math.pow(react2Skill(timeToReact), Variables.Reaction.CurveExp);
}

function findTimingAt(
    _timings: ITiming[],
    _time: number
): int
{
	let start = 0;
	let end = _timings.length - 2;
	let mid: int;

	if (end < 0)
		return 0;

	while (start <= end)
	{
		mid = Math.floor((start + end) / 2);

		if (BTWN(_timings[mid].time, _time, _timings[mid + 1].time))
			return mid + 1;

		if (_time < _timings[mid].time)
			end = mid - 1;
		else
			start = mid + 1;
	}

	if (_time < _timings[0].time)					return Number.MIN_SAFE_INTEGER;
	if (_time > _timings[_timings.length - 1].time) return Number.MAX_SAFE_INTEGER;

	return NaN;
}

/**
 * Original model can be found at https://www.desmos.com/calculator/lg2jqyesnu
 * @param _timeToReact 
 */
function react2Skill(_timeToReact: number): number
{
	let a = Math.pow(2.0, Math.log(78608.0 / 15625.0) / Math.log(34.0 / 25.0))*Math.pow(125.0, Math.log(68.0 / 25.0) / Math.log(34.0 / 25.0));
	let b = Math.log(2.0) / (Math.log(2.0) - 2.0*Math.log(5.0) + Math.log(17.0));
	return a / Math.pow(_timeToReact, b);
}

/**
 * Original model can be found at https://www.desmos.com/calculator/k9r2uipjfq
 * @param p1 
 * @param p2 
 * @param p3 
 * @param ARms 
 * @param CSpx 
 */
function pattern2Reaction(
    p1: ITiming,
    p2: ITiming,
    p3: ITiming,
    ARms: number,
    CSpx: number
): number
{
	let damping = Variables.Reaction.PatternDamping;
	let curveSteepness = /*(300.0 / (ARms + 250.)) **/ damping;
	let patReq = PatternReq(p1, p2, p3, CSpx);

	return ARms - ARms * Math.exp(-curveSteepness * patReq) /*+ curveSteepness*sqrt(curveSteepness*patReq)*/;
}

function PatternReq(
    p1: ITiming,
    p2: ITiming,
    p3: ITiming,
    CSpx: number
): number
{
	let point1 = new Vector2(p1.pos.X, p1.pos.Y);
	let point2 = new Vector2(p2.pos.X, p2.pos.Y);
	let point3 = new Vector2(p3.pos.X, p3.pos.Y);

	let dist_12 = Vector2.distance(point1, point2);
	let dist_23 = Vector2.distance(point2, point3);
	let dist = dist_12 + dist_23;

	let angle = GetAngle(point1,point2,point3);

	let time = Math.abs(p3.time - p1.time);
	time = (time < 16) ? 16 : time; // 16ms @ 60 FPS

	// 2 * _CSpx = 1 diameter od CS since CS here is being calculated in terms of radius
	return time / ((dist / (2 * CSpx)) * ((ActualPI - angle) / ActualPI));  
}
