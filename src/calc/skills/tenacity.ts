import { IBeatmap, } from "../../beatmap";
import { Variables } from "../../vars";
import { int, } from "../../utils";

export function calculateTenacity(beatmap: IBeatmap) {
    let longestStream = GetLongestStream(beatmap.streams);

    let a = Math.pow(longestStream.interval, Variables.Tenacity.IntervalPow);
    let b = Math.pow(longestStream.interval, a * Variables.Tenacity.IntervalMult);
    let intervalScaled = 1.0 / b * Variables.Tenacity.IntervalMult2;
    let c = Variables.Tenacity.LengthDivisor / longestStream.length * Variables.Tenacity.LengthMult;
    let lengthScaled = Math.pow(Variables.Tenacity.LengthDivisor / longestStream.length, c);

    return Variables.Tenacity.TotalMult * Math.pow(intervalScaled * lengthScaled, Variables.Tenacity.TotalPow);
}

class Stream {
    //  duration
    interval: number;

    //  number of notes
    length: number;

    constructor(interval: number, length: number) {
        this.interval = interval;
        this.length = length;
    }
}

function GetLongestStream(streams: any): Stream
{
    //  Map<number, Array<Array<number>>>
	let max = 1;
	let interval = 0;

    Object.keys(streams).forEach(key => {
        interval = Number.parseInt(key);
        max = 1;

        let second = streams[key];
        second.forEach(j => {
            //  TODO: use for/while here
            //  so we dont run through the whole array lol
            if(max > 1)
                return;

            let length: int = j.length + 1;
			if (length > max)
				max = length;
        });
    });

    return new Stream(interval, max);
}
