import { erf, } from "mathjs";

import { IBeatmap, } from "../../beatmap";
import ModType from "../../mod-type";
import { double, int, logf, OD2ms } from "../../utils";
import { Variables } from "../../vars";

export function calculateAccuracy(beatmap: IBeatmap, stamina: double): double {
    let circles: int = beatmap.circles.length;
    let od_ms = OD2ms(beatmap.od);

    if (beatmap.hasMod(ModType.DT)) od_ms /= 1.5;
    else if (beatmap.hasMod(ModType.HT)) od_ms /= 0.75;
    
    //std::cout << "circles = " << circles << "   od_ms: " << od_ms << "    stamina = " << stamina << "\n";

    let tapping: double = 0;
    if(stamina === 0)
        tapping = erf(Number.POSITIVE_INFINITY);
    else
        tapping = erf(od_ms / (Variables.Accuracy.AccScale * stamina * stamina));

    return -Variables.Accuracy.VerScale * circles * logf(tapping);
}