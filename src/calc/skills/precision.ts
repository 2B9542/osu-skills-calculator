import { double } from "../../utils";
import { IBeatmap, } from "../../beatmap";
import { Variables } from "../../vars";

export function calculatePrecision(beatmap: IBeatmap, agility: double): double {
    /*
        let scaledAgility = agility > Variables.Precision.AgilityLimit
        ? 1 
        : 0;

        + scaledAgility instead of + 1 ??
    */
    let scaledAgility = Math.pow(agility + 1, Variables.Precision.AgilityPow) - Variables.Precision.AgilitySubtract;

    // the magic number above is to make an agility of 10 become 1 when scaled
    return Variables.Precision.TotalMult * Math.pow(scaledAgility * beatmap.cs, Variables.Precision.TotalPow);
}
