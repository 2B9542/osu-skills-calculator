import { IBeatmap, } from "../../beatmap";
import { getWeightedValue2, getPeakVals, double } from "../../utils";
import { Variables } from "../../vars";

export function calculateAgility(beatmap: IBeatmap): double {
    let topWeights = [];
    getPeakVals(beatmap.aimStrains, topWeights);

    let agility = getWeightedValue2(topWeights, Variables.Agility.Weighting);
    agility = Variables.Agility.TotalMult * Math.pow(agility, Variables.Agility.TotalPow);

    return agility;
}
