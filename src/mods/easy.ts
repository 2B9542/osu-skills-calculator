import { IBeatmap } from "../beatmap";
import { IMod } from "./mod";

class _Easy implements IMod {
    public apply(beatmap: IBeatmap) {
        beatmap.ar *= 0.5;
		beatmap.od *= 0.5;
		beatmap.cs *= 0.5;
    }
}

const Easy = new _Easy();
export default Easy;