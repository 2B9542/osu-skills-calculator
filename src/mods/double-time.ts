import { AR2ms, ms2AR } from "../utils";
import { IBeatmap } from "../beatmap";
import { IMod } from "./mod";

class _DoubleTime implements IMod {
    public apply(beatmap: IBeatmap) {
        beatmap.hitObjects.forEach(obj => {
			obj.time = Math.ceil(obj.time / 1.5);
        });

        beatmap.timingPoints.forEach(timings => {
            if(timings.beatInterval > 0)
				timings.beatInterval /= 1.5;
			timings.offset = Math.ceil(timings.offset / 1.5);
        });

		beatmap.ar = Math.min(ms2AR(Math.floor(AR2ms(beatmap.ar) / 1.5)), 11.0);
		// beatmap.od = beatmap.od * 1.5;
    }
}

const DoubleTime = new _DoubleTime();
export default DoubleTime;