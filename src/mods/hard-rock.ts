import { IBeatmap } from "../beatmap";
import { IMod } from "./mod";

class _HardRock implements IMod {
    public apply(beatmap: IBeatmap) {
        if (beatmap.ar * 1.4 < 10)
            beatmap.ar *= 1.4;
        else
            beatmap.ar = 10;

        if (beatmap.od * 1.4 < 10)
            beatmap.od *= 1.4;
        else
            beatmap.od = 10;

		beatmap.cs = Math.min(10.0, beatmap.cs * 1.3);
    }
}

const HardRock = new _HardRock();
export default HardRock;