import { IBeatmap } from "../beatmap";

export interface IMod {
    apply(beatmap: IBeatmap): void;
}