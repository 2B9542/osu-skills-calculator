import { AR2ms, ms2AR } from "../utils";
import { IBeatmap } from "../beatmap";
import { IMod } from "./mod";

class _HalfTime implements IMod {
    public apply(beatmap: IBeatmap) {
        beatmap.hitObjects.forEach(obj => {
			obj.time = Math.ceil(obj.time / 0.75);
        });

        beatmap.timingPoints.forEach(timings => {
            if(timings.beatInterval > 0)
				timings.beatInterval /= 0.75;
			timings.offset = Math.ceil(timings.offset / 0.75);
        });

		beatmap.ar = ms2AR(Math.floor(AR2ms(beatmap.ar) / 0.75));
    }
}

const HalfTime = new _HalfTime();
export default HalfTime;