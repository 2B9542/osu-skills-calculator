import HitObjectType from "./hit-object-type";
import { IsHitObjectType } from "./utils";

export function run() {
    let types = Object.keys(HitObjectType).slice(8);

    types.forEach(type => {
        types.forEach(other => {
            if(IsHitObjectType(HitObjectType[type], HitObjectType[other]))
                console.log(`${type} is ${other}`);
            else
                console.log(`${type} is not ${other}`);
        });
    });
}
