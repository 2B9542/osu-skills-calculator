import { IBeatmap, ISkills, } from "./beatmap";

import { calculateStamina } from "./calc/skills/stamina";
import { calculateTenacity } from "./calc/skills/tenacity";
import { calculateAccuracy } from "./calc/skills/accuracy";
import { calculateAgility } from "./calc/skills/agility";
import { calculatePrecision } from "./calc/skills/precision";
import { calculateReaction } from "./calc/skills/reaction";

import { calculateAimStrains } from "./calc/aim-strains";
import { ParseBeatmap } from "./reader";
import ModType from "./mod-type";
import { calculateTapStrains } from "./calc/tap-strains";
import { prepareTimingPoints } from "./prep/prepare-timing-points";
import { approximateSliderPoints } from "./prep/approximate-slider-points";
import { bakeSliderData } from "./prep/bake-slider-data";
import { prepareAimData } from "./prep/prepare-aim-data";
import { prepareTapData } from "./prep/prepare-tap-data";
import { double, int } from "utils";

export function PreprocessMap(beatmap: IBeatmap): boolean
{
    if (beatmap.hitObjects.length < 2)
    {
        console.log(`${beatmap.name}: map has less than 2 hit objects!`);
        return false;
    }

    prepareTimingPoints(beatmap);
    approximateSliderPoints(beatmap);
    bakeSliderData(beatmap);

    prepareAimData(beatmap);
    prepareTapData(beatmap);

    if (beatmap.distances.length === 0)
        return false;

    return true;
}

function calculateSkills(beatmap: IBeatmap): ISkills
{
    let reaction = calculateReaction(beatmap);
    let stamina = calculateStamina(beatmap);
    let tenacity = calculateTenacity(beatmap);
    // let agilityV2 = false;
    // if (agilityV2)
    //     CalculateAgilityStrains(beatmap);  // calculates precision as well. Might seperate that later
    // else
    let agility = calculateAgility(beatmap);
    let precision = calculatePrecision(beatmap, agility);
    let accuracy = calculateAccuracy(beatmap, stamina);
    // if (hasMod(beatmap, ModType.FL))
    // {
    //     CalculateMemory(beatmap);
    // }
    // CalculateReading(beatmap, hasMod(beatmap, HD));

    return {
        accuracy: accuracy,
        tenacity: tenacity,
        precision: precision,
        agility: agility,
        stamina: stamina,
        reaction: reaction,
    };
}

export function CalculateBeatmapSkills(
    filepath: string,
    mods: number
): ICalculationResult | null
{
    let beatmap: IBeatmap = ParseBeatmap(filepath);
    
    try {
        let modsList = getModsList(mods);

        beatmap.applyMods(modsList);
    } catch(error) {
        //  TODO: handle it
        throw (error);
    }

    if(!PreprocessMap(beatmap))
        throw new Error(`${beatmap.name}: failed preprocessing`);

    beatmap.aimStrains = calculateAimStrains(beatmap);
    beatmap.tapStrains = calculateTapStrains(beatmap);
    beatmap.skills = calculateSkills(beatmap);

    let sliderCount: int = beatmap.sliders.length;
    let spinnerCount: int = beatmap.spinners;
    let circleCount: int = beatmap.circles.length;

    return {
        circles: circleCount,
        sliders: sliderCount,
        spinners: spinnerCount,
        skills: beatmap.skills,
        name: beatmap.name,
        ar: beatmap.ar,
        cs: beatmap.cs,
    };
}

interface ICalculationResult {
    circles: int,
    sliders: int,
    spinners: int,
    skills: ISkills,
    name: string,
    ar: double,
    cs: double,
}

function getModsList(mods: number): ModType[] {
    function hasMod(mod: number) {
        return (mod & mods) > 0;
    }

    let result: ModType[] = [];

    if (hasMod(ModType.EZ) && hasMod(ModType.HR))
		throw new Error(`invalid mods: EZ + HR`);
        
    if (hasMod(ModType.HT) && hasMod(ModType.DT))
		throw new Error(`invalid mods: HT + DT`);
        
	if (hasMod(ModType.EZ))
        result.push(ModType.EZ);
        
    if (hasMod(ModType.HR))
        result.push(ModType.HR);

    if (hasMod(ModType.HT))
        result.push(ModType.HT);
        
    if (hasMod(ModType.DT))
		result.push(ModType.DT);

    if (hasMod(ModType.HD))
		result.push(ModType.HD);
    
    if (hasMod(ModType.FL))
        result.push(ModType.FL);

    return result;
}
