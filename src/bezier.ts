import { Vector2 } from "./hit-objects/vector2";
import { bernstein, double, int, uint } from "./utils";

export interface IBezier {

    /**
     * Returns the number of points along the curve.
     */
    getCurvesCount(): int;

    /**
     * Returns the distances between a point of the curve and the last point.
     */
    getCurveDistances(): double[];

    /**
     * Returns the points along the curve of the Bezier curve.
     */
    getCurvePoint(): Vector2[];

    pointAt(t: double): Vector2;

    /**
     * Returns the total distances of this Bezier curve.
     */
    TotalDistance(): double;
}

export class Bezier implements IBezier {

    /**
     * The control points of the Bezier curve
     */
    private points: Vector2[] = [];

    /**
     * Points along the curve of the Bezier curve
     */
    private curvePoints: Vector2[] = [];

    /**
     * Distances between a point of the curve and the last point
     */
    private curveDis: double[] = [];

    /**
     * The number of points along the curve
     */
    private ncurve: int = 0;

    /**
     * The total distances of this Bezier
     *  */ 
    private totalDistance: double = 0;

    constructor(points: Vector2[]) {
        this.points = Array.from(points);

        // approximate by finding the length of all points
        // (which should be the max possible length of the curve)
        let approxlength: double = 0;
        for (let i: uint = 0; i < points.length - 1; i++)
            approxlength += Vector2.distance(points[i], points[i + 1]);
    
        this.init(approxlength);
    }

    private init(approxLength: double) {

        // subdivide the curve
        this.ncurve = Math.floor(approxLength / 4.0) + 2;
        for (let i: int = 0; i < this.ncurve; i++)
            this.curvePoints.push(this.pointAt(i / (this.ncurve - 1)));

        // find the distance of each point from the previous point
        //curveDis = new double[this.ncurve];
        this.totalDistance = 0;
        for (let i: int = 0; i < this.ncurve; i++) {
            this.curveDis.push((i == 0) ? 0 : Vector2.distance(this.curvePoints[i], this.curvePoints[i - 1]));
            this.totalDistance += this.curveDis[i];
        }
    }

    public getCurvesCount(): int {
        return this.ncurve;
    }

    public getCurveDistances(): double[] {
        return this.curveDis;
    }

    public getCurvePoint(): Vector2[] {
        return this.curvePoints;
    }

    public TotalDistance(): double {
        return this.totalDistance;
    }

    public pointAt(t: number): Vector2 {
        let c: Vector2 = new Vector2();
        let n: int = this.points.length - 1;
        for (let i: int = 0; i <= n; i++) 
        {
            let b: double = bernstein(i, n, t);
            c.X += this.points[i].X * b;
            c.Y += this.points[i].Y * b;
        }
        return c;
    }
}