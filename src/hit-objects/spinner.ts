import { HitObject, IHitObject } from "./base";
import { Vector2 } from "./vector2";

export interface ISpinner extends IHitObject {
    endTime: number;
}

export class Spinner extends HitObject implements ISpinner {
    position: Vector2;
    type: number;
    time: number;

    endTime: number;
}