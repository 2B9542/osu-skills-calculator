import { ActualPI, double, int, lerp, uint } from "../utils";
import { IHitObject } from "./base";
import { CURVE_POINTS_SEPERATION, ISlider, Slider } from "./slider";
import { Vector2 } from "./vector2";

const TWO_PI: double = ActualPI * 2.;
const HALF_PI: double = ActualPI / 2.;

export class CircumscribedCircle {

    /**
     * Points along the curve (set by inherited classes)
     */
    public curves: Vector2[] = [];
    /**
     * The number of points along the curve
     */
    public ncurve: int = 0;

    private center: Vector2 = new Vector2();
    private radius: double = 0;

//#region The three points to create the Circumscribed Circle from
    private start: Vector2 = new Vector2();
    private mid: Vector2 = new Vector2();
    private end: Vector2 = new Vector2();
//#endregion

//#region The three angles relative to the circle center
    private startAngle: double = 0;
    private midAngle: double = 0;
    private endAngle: double = 0;
//#endregion

    public temporaryFallback: boolean = false;

    constructor(hitObject: ISlider) {

        let len = hitObject.curves.length;
        for (let i: uint = 0; i < len; i++)
        {
            hitObject.sliderX.push(hitObject.curves[i].X);
            hitObject.sliderY.push(hitObject.curves[i].Y);
        }
        hitObject.x = hitObject.position.X;
        hitObject.y = hitObject.position.Y;

        // construct the three points
        this.start = new Vector2(hitObject.getX(0), hitObject.getY(0));
        this.mid = new Vector2(hitObject.getX(1), hitObject.getY(1));
        this.end = new Vector2(hitObject.getX(2), hitObject.getY(2));

        // find the circle center
        let mida: Vector2 = this.start.midPoint(this.mid);
        let midb: Vector2 = this.end.midPoint(this.mid);
        let nora: Vector2 = this.mid.minus(this.start).nor;
        let norb: Vector2 = this.mid.minus(this.end).nor;

        this.center = this.intersect(mida, nora, midb, norb);
        if (this.center == new Vector2(-1, -1))
        {
            // Temporary fallback to bezier slider
            this.temporaryFallback = true;
            return;
        }

        // find the angles relative to the circle center
        let startAngPoint: Vector2 = this.start.minus(this.center);
        let midAngPoint: Vector2 = this.mid.minus(this.center);
        let endAngPoint: Vector2 = this.end.minus(this.center);

        this.startAngle = Math.atan2(startAngPoint.Y, startAngPoint.X);
        this.midAngle = Math.atan2(midAngPoint.Y, midAngPoint.X);
        this.endAngle = Math.atan2(endAngPoint.Y, endAngPoint.X);

        // find the angles that pass through this.midAngle
        if (!this.isIn(this.startAngle, this.midAngle, this.endAngle))
        {
            if (Math.abs(this.startAngle + TWO_PI - this.endAngle) < TWO_PI && this.isIn(this.startAngle + (TWO_PI), this.midAngle, this.endAngle))
                this.startAngle += TWO_PI;
            else if (Math.abs(this.startAngle - (this.endAngle + TWO_PI)) < TWO_PI && this.isIn(this.startAngle, this.midAngle, this.endAngle + (TWO_PI)))
                this.endAngle += TWO_PI;
            else if (Math.abs(this.startAngle - TWO_PI - this.endAngle) < TWO_PI && this.isIn(this.startAngle - (TWO_PI), this.midAngle, this.endAngle))
                this.startAngle -= TWO_PI;
            else if (Math.abs(this.startAngle - (this.endAngle - TWO_PI)) < TWO_PI && this.isIn(this.startAngle, this.midAngle, this.endAngle - (TWO_PI)))
                this.endAngle -= TWO_PI;
            else
            {
                console.error(`Cannot find angles between ${this.startAngle}, ${this.midAngle}, ${this.endAngle}.`);
                return;
            }
        }

        // find an angle with an arc length of pixelLength along this circle
        this.radius = startAngPoint.length;
        let pixelLength: double = hitObject.pixelLength;
        let arcAng: double = pixelLength / this.radius;  // len = theta * r / theta = len / r

        // now use it for our new end angle
        this.endAngle = (this.endAngle > this.startAngle) ? this.startAngle + arcAng : this.startAngle - arcAng;

        // calculate points
        let step: double = hitObject.pixelLength / CURVE_POINTS_SEPERATION;
        this.ncurve = Math.floor(step);
        len = Math.floor(step + 1);
        for (let i: int = 0; i < len; i++)
        {
            let xy: Vector2 = this.pointAt(i / step);
            this.curves.push(new Vector2(xy.X, xy.Y));
        }
    }


    /**
	* Finds the point of intersection between the two parametric lines
	* {@code A = a + ta*t} and {@code B = b + tb*u}.
	* http://gamedev.stackexchange.com/questions/44720/
	* @param a  the initial position of the line A
	* @param ta the direction of the line A
	* @param b  the initial position of the line B
	* @param tb the direction of the line B
	* @return the point at which the two lines intersect
	*/
	public intersect(
        a: Vector2,
        ta: Vector2,
        b: Vector2,
        tb: Vector2
    ): Vector2
    {
        let des: double = tb.X * ta.Y - tb.Y * ta.X;
        if (Math.abs(des) < 0.00001)
            //  parallel
            return new Vector2(-1, -1);

        let u: double = ((b.Y - a.Y) * ta.X + (a.X - b.X) * ta.Y) / des;
        b.X += tb.X * u;
        b.Y += tb.Y * u;

        return b;
    }

    /**
	* Checks to see if "b" is between "a" and "c"
	* @return true if b is between a and c
	*/
    public isIn(
        a: double,
        b: double,
        c: double
    ): boolean
    {
        return (b > a && b < c) || (b < a && b > c);
    }

    public pointAt(t: double): Vector2 {
        let ang: double = lerp(this.startAngle, this.endAngle, t);
        return new Vector2(
            (Math.cos(ang) * this.radius + this.center.X),
            (Math.sin(ang) * this.radius + this.center.Y)
        );
    }
}
