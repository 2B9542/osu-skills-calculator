import { clamp } from "../utils";

export class Vector2 {
    constructor(public X: number = 0, public Y: number = 0) {

    }

	public get length(): number {
        return Math.sqrt(this.X * this.X + this.Y * this.Y);
    }

    public midPoint(other: Vector2): Vector2 {
		return new Vector2((this.X + other.X) / 2., (this.Y + other.Y) / 2.);
    }

	public get nor(): Vector2 {
		return new Vector2(-this.Y, this.X);
	}

    public get angle(): number {
        let X = this.X;
        let Y = this.Y;

		if (Y == 0) // corrected thanks to a suggestion by Jox
			return X < 0 ? 180 : 0;
		else if (X == 0)
			return Y < 0 ? 90 : 270;

		// don't use getLength here to avoid precision loss with s32 vectors
		// avoid floating-point trouble as sqrt(y*y) is occasionally larger than y, so clamp
		const tmp = clamp(Y / Math.sqrt(X * X + Y * Y), -1.0, 1.0);
		const angle = Math.atan(Math.sqrt(1 - tmp * tmp) / tmp) * (180.0 / 3.1415926535897932384626433832795028841971693993751);

		if (X > 0 && Y > 0)
			return angle + 270;
		else
		if (X > 0 && Y < 0)
			return angle + 90;
		else
		if (X < 0 && Y < 0)
			return 90 - angle;
		else
		if (X < 0 && Y > 0)
			return 270 - angle;

		return angle;
    }
    
    public plus(other: Vector2) {
        return new Vector2(
            this.X + other.X,
            this.Y + other.Y,
        );
    }

    public minus(other: Vector2) {
        return this.plus(new Vector2(-other.X, -other.Y));
    }

    static distance(
        first: Vector2, 
        second: Vector2
    ): number {
		return new Vector2(first.X - second.X, first.Y - second.Y).length;
    }
}
