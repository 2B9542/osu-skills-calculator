import HitObjectType from "../hit-object-type";
import { boolean } from "mathjs";
import ModType from "../mod-type";
import { AR2ms, getValue, makePair, Pair } from "../utils";
import { Vector2 } from "./vector2";

export interface IHitObject {
    position: Vector2;
    type: number;
    time: number;

    getVisibilityTimes(
        options: IVisibilityTimesOptions
    ): Pair<number, number>;

    getVisibilityTimeWithHD(
        options: IVisibilityTimeWithHDOptions
    ): number;

    getVisibilityTimeWithoutHD(): number;

    isType(expected: HitObjectType): boolean;
    isCircle(): boolean;
    isSlider(): boolean;
}

export interface IVisibilityTimesOptions {
    AR: number,
    mods?: ModType[],
    hidden: boolean,    //  temporary
    opacityStart: number,
    opacityEnd: number,
}

export interface IVisibilityTimeWithHDOptions {
    fadeInTimeEnd: number,
    opacityStart: number,
    opacityEnd: number,
}

export class HitObject implements IHitObject {
    position: Vector2 = new Vector2();
    type: number;
    time: number;

    public getVisibilityTimes(
        options: IVisibilityTimesOptions = {
            AR: 9,
            hidden: false,
            opacityStart: 0.0,
            opacityEnd: 1.0,
        }
    ): Pair<number, number> {
        //  Time when the AR goes into effect
        let AR = options.AR;
        // let hidden = options.mods.includes(ModType.HD);
        let hidden = options.hidden;
        let opacityStart = options.opacityStart;
        let opacityEnd = options.opacityEnd;
        
        let preampTime: number = this.time - AR2ms(AR);
        let times: Pair<number, number> = makePair(0, 0);

        if(hidden) {
            //  how long the fadein period is
            let fadeinDuration: number = 0.4 * AR2ms(AR);
            //  when it is fully faded in
            let fadeInTimeEnd: number = preampTime + fadeinDuration;

            times.first = Math.floor(getValue(preampTime, fadeInTimeEnd, opacityStart));

            times.second = this.getVisibilityTimeWithHD({
                fadeInTimeEnd,
                opacityStart,
                opacityEnd,
            });

            return times;
        }

        //  how long the fadein period is
        let fadeInDuration: number = Math.min(AR2ms(AR), 400);
        //  when it is fully faded in
		let fadeinTimeEnd: number = preampTime + fadeInDuration;

        // Fadein period always lasts from preamp time to 400 ms after preamp time or
		// when the object needs to be hit, which ever is smaller
        times.first = Math.floor(getValue(preampTime, fadeinTimeEnd, opacityStart));
        times.second = this.getVisibilityTimeWithoutHD();

        return times;
    }

    public getVisibilityTimeWithHD(
        options: IVisibilityTimeWithHDOptions
    ): number 
    {
        let time = this.time;
        let fadeInTimeEnd = options.fadeInTimeEnd;
        let opacityStart = options.opacityStart;
        
        //  how long the fadeout period is
        //  when it is fully faded out
        let fadeoutDuration: number = 0.7 * (time - fadeInTimeEnd);
        let fadeoutTimeEnd: number = fadeInTimeEnd + fadeoutDuration;

        return Math.floor(getValue(fadeInTimeEnd, fadeoutTimeEnd, 1.0 - opacityStart)); // <-- no this is not a mistake :D
    }

    public getVisibilityTimeWithoutHD(
    ): number {
		return this.time;
    }

    public isType(expected: HitObjectType): boolean {
        return boolean(this.type & expected) as boolean;
    }

    public isCircle(): boolean {
        return this.isType(HitObjectType.Normal);
    }

    public isSlider(): boolean {
        return this.isType(HitObjectType.SLIDER);
    }
}