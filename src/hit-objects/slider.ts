import { Bezier, IBezier } from "../bezier";
import { clear, double, getValue, int, lerp, uint } from "../utils";
import { HitObject, IHitObject, IVisibilityTimeWithHDOptions } from "./base";
import { Vector2 } from "./vector2";

/** Points generated along the curve should be spaced this far apart. */
export const CURVE_POINTS_SEPERATION: int = 5;

export interface ISlider extends IHitObject {
    curveType: string;
    /**
     * from file
     */
    curves: Vector2[];
    /**
     * calculated
     */
    lerpPoints: Vector2[];
    /**
     * number of calculated points
     */
    ncurve: number;
    /**
     * number of repeats
     */
    repeat: number;
    /**
     * the time of repeats
     */
    repeatTimes: number[];
    pixelLength: number;
    repeatTime: number;
    endTime: number;
    /**
     * time to travel to reverse arrow
     */
    toRepeatTime: number;
    /**
     * calculated
     */
    endPoint: Vector2;
    ticks: number[];

    /** The scaled starting x coordinate. */
    x: double;

    /** The scaled starting y coordinate. */
    y: double;

    /** The scaled slider x coordinate list. */
    sliderX: double[];

    /** The scaled slider y coordinate list. */
    sliderY: double[];

    getX(i: int): double;
    getY(i: int): double;

    /**
     * Vector2 GetSliderPos(HitObject, int);
     */
    getPositionAt(time: int): Vector2;

    generateCurves(line: boolean): void;

    /**
     * Saving the time of repeats
     */
    processRepeatTimes(): void;
    processTickTimestamps(beatLength: number, tickRate: number): void;
}

export class Slider extends HitObject implements ISlider {
    curveType: string;
    curves: Vector2[] = [];
    lerpPoints: Vector2[] = [];
    ncurve: number;
    repeat: number;
    repeatTimes: number[] = [];
    pixelLength: number;
    repeatTime: number;
    endTime: number;
    toRepeatTime: number;
    endPoint: Vector2;
    ticks: number[] = [];

    /** The scaled starting x coordinate. */
    public x: double = 0;

    /** The scaled starting y coordinate. */
    public y: double = 0;

    /** The scaled slider x coordinate list. */
    public sliderX: double[] = [];

    /** The scaled slider y coordinate list. */
    public sliderY: double[] = [];


    constructor(options?: {
        hitObject: ISlider,
        line: boolean,
    }) {
        super();

        if(options === undefined || options === null)
            return;
    }

    public getPositionAt(time: int): Vector2 {
        // convert time to percent
        let percent: double;
        if (time <= this.time)
            percent = 0;
        else if (time > this.endTime)
            percent = 1;
        else
        {
            let timeLength: int = (time - this.time);
            let repeatsDone: int = Math.floor(timeLength / this.toRepeatTime);
            percent = (timeLength - this.toRepeatTime * repeatsDone) / this.toRepeatTime;
            if (repeatsDone % 2)
                percent = 1 - percent; // it's going back
        }
        
        // get the points
        let ncurve: int = this.ncurve;
        let indexF: double = percent * ncurve;
        let index: int = Math.floor(indexF);

        if(ncurve <= 0)
            return new Vector2(-1, -1);

        if (index >= this.ncurve)
        {
            let poi: Vector2 = this.lerpPoints[ncurve - 1];
            return new Vector2(poi.X, poi.Y);
        }
        else
        {
            let poi: Vector2 = this.lerpPoints[index];
            let poi2: Vector2 = this.lerpPoints[index + 1];
            let t2: double = indexF - index;
            return new Vector2(lerp(poi.X, poi2.X, t2), lerp(poi.Y, poi2.Y, t2));
        }
    }

    public getX(i: int): double {
        return (i === 0) ? this.x : this.sliderX[i - 1];
    }

    public getY(i: int): double {
        return (i === 0) ? this.y : this.sliderY[i - 1];
    }

    public getVisibilityTimeWithHD(
        options: IVisibilityTimeWithHDOptions
    ): number {
        // If it's a slider, then the fade out period lasts from when it's fadedin to
        // 70% to the time it the slider ends

        let endTime = this.endTime;
        let fadeInTimeEnd = options.fadeInTimeEnd;
        let opacityEnd = options.opacityEnd;
        
        //  how long the fadeout period is
        let fadeoutDuration: number = endTime - fadeInTimeEnd;
        //  when it is fully faded out
        let fadeoutTimeEnd: number = fadeInTimeEnd + fadeoutDuration;

        return Math.floor(getValue(fadeInTimeEnd, fadeoutTimeEnd, 1.0 - opacityEnd));
    }

    public getVisibilityTimeWithoutHD(

    ): number {
        return this.endTime;
    }

    public generateCurves(line: boolean): void {
        let beziers: IBezier[] = [];

        /*
        // Beziers: splits points into different Beziers if has the same points (red points)
        // a b c - c d - d e f g
        // Lines: generate a new curve for each sequential pair
        // ab  bc  cd  de  ef  fg
        */

        let controlPoints: int = this.curves.length + 1;
        let points: Vector2[] = [];  // temporary list of points to separate different Bezier curves
        let lastPoi: Vector2 = new Vector2(-1, -1);

        this.curves.forEach(curve => {
            this.sliderX.push(curve.X);
            this.sliderY.push(curve.Y);
        })
    
        this.x = this.position.X;
        this.y = this.position.Y;

        for (let i = 0; i < controlPoints; i++)
        {
            let tpoi: Vector2 = new Vector2(
                this.getX(i),
                this.getY(i)
            );

            if (line) 
            {
                //  TODO: === == operator overloading for Vector2
                if (lastPoi !== new Vector2(-1, -1))
                {
                    points.push(tpoi);
                    beziers.push(new Bezier(points));
                    clear(points);
                }
            }
            else if ((lastPoi !== new Vector2(-1, -1)) && (tpoi === lastPoi))
            {
                if (points.length >= 2)
                    beziers.push(new Bezier(points));
                clear(points);
            }

            points.push(tpoi);
            lastPoi = tpoi;
        }

        if (line || points.length < 2) 
        {
            // trying to continue Bezier with less than 2 points
            // probably ending on a red point, just ignore it
        }
        else 
        {
            beziers.push(new Bezier(points));
            clear(points);
        }

    	this.init(beziers);
    }

    private init(curves: IBezier[]): void {
        // now try to create points the are equidistant to each other
        this.ncurve = Math.floor(this.pixelLength / CURVE_POINTS_SEPERATION);

        // if the slider has no curve points, force one in 
        // a hitobject that the player holds must have at least one point
        if (curves.length == 0) {
            curves.push(new Bezier([this.position]));
            this.endPoint = this.position;
        }

        let distanceAt: double = 0;
        let curveCounter: uint = 0;
        let curPoint: int = 0;
        let curCurve: IBezier = curves[curveCounter++];
        let lastCurve: Vector2 = curCurve.getCurvePoint()[0];
        let lastDistanceAt: double = 0;

        // length of Curve should equal pixel length (in 640x480)
        let pixelLength: double = this.pixelLength;

        // for each distance, try to get in between the two points that are between it
        for (let i: int = 0; i < this.ncurve + 1; i++) 
        {
            let prefDistance: int = Math.floor(i * pixelLength / this.ncurve);
            while (distanceAt < prefDistance)
            {
                lastDistanceAt = distanceAt;
                lastCurve = curCurve.getCurvePoint()[curPoint];
                curPoint++;

                if (curPoint >= curCurve.getCurvesCount())
                {
                    if (curveCounter < curves.length)
                    {
                        curCurve = curves[curveCounter++];
                        curPoint = 0;
                    }
                    else 
                    {
                        curPoint = curCurve.getCurvesCount() - 1;
                        
                        // out of points even though the preferred distance hasn't been reached
                        if (lastDistanceAt == distanceAt) break;	
                    }
                }
                distanceAt += curCurve.getCurveDistances()[curPoint];
            }
            let thisCurve: Vector2 = curCurve.getCurvePoint()[curPoint];

            // interpolate the point between the two closest distances
            if (distanceAt - lastDistanceAt > 1) 
            {
                let t: double = (prefDistance - lastDistanceAt) / (distanceAt - lastDistanceAt);
                this.curves[i] = new Vector2(
                    lerp(lastCurve.X, thisCurve.X, t),
                    lerp(lastCurve.Y, thisCurve.Y, t)
                );
            }
            else
                this.curves[i] = thisCurve;
        }
    }

    processRepeatTimes() {
        if(this.repeat <= 1)
            return;

        for (let i = this.time; i < this.endTime; i += this.toRepeatTime)
        {
            if (i > this.endTime)
                break;

            this.repeatTimes.push(i);
        }
    }

    processTickTimestamps(beatLength: number, tickRate: number): void {
        let tickInterval = Math.floor(beatLength / tickRate);
        const errInterval = 10;
        let j = 1;

        for (let i = this.time + tickInterval; i < (this.endTime - errInterval); i += tickInterval)
        {
            if (i > this.endTime) break;
            
            let tickTime = this.time + Math.floor(tickInterval * j);
            if (tickTime < 0) break;
            
            this.ticks.push(tickTime);
            j++;
        }
    }
}

export function GetLastTickTime(hitObj: ISlider): int {
	if (!hitObj.ticks.length)
	{
		if (hitObj.repeat > 1)
			return Math.floor(hitObj.endTime - (hitObj.endTime - hitObj.repeatTimes[hitObj.repeatTimes.length - 1]) / 2.0);
		else
			return Math.floor(hitObj.endTime - (hitObj.endTime - hitObj.time) / 2.0);
	}
	else
		return Math.floor(hitObj.endTime - (hitObj.endTime - hitObj.ticks[hitObj.ticks.length - 1]) / 2.0);
}