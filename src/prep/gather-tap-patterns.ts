import { IBeatmap } from "../beatmap";
import { clear, int } from "../utils";

//  NOTSURE: might have an issue with the Maps
export function gatherTapPatterns(beatmap: IBeatmap): void {
    let sections: any = {};
	// Get sections of 1/2 1/4 e.t.c.

	let tmp: int[] = [];
	let uniq: Set<int> = new Set();
	let old: int = 0;
	let i: int = 0;
    const OFFSET_MAX_DISPLACEMENT: int = 2;

    beatmap.pressIntervals.forEach(interval => {
        let uniqArray = Array.from(uniq);
        let it = uniqArray.findIndex(i => i === interval);
        if(it === -1) {
            let found: boolean = false;
			for(let p: int = (interval - OFFSET_MAX_DISPLACEMENT); p <= (interval + OFFSET_MAX_DISPLACEMENT); p++)
			{
				let it2 = uniqArray.findIndex(i => i === p);
				if(it2 !== -1)
				{
					interval = p;
					found = true;
					break;
				}
            }

			if(!found)
			{
				uniq.add(interval);
				sections[interval] = new Array<Array<int>>();
				beatmap.streams.set(interval, new Array<Array<int>>());
				beatmap.bursts.set(interval, new Array<Array<int>>());
			}
        }

        if(Math.abs(interval - old) > OFFSET_MAX_DISPLACEMENT)
		{
			// disregard everything shorter than a triple
			if(tmp.length > 1)
			{
                let sec = sections[old];
                sec?.push(tmp);

				if(tmp.length > 6)
					beatmap.streams.get(old)?.push(tmp);
				else
					beatmap.bursts.get(old)?.push(tmp);
			}
			clear(tmp);
        }

		tmp.push(beatmap.hitObjects[i].time);
		old = interval;
		i++;
    });

	if(tmp.length > 0)
	{
		// disregard everything shorter than a triple
		if(tmp.length > 1)
		{
			sections[old]?.push(tmp);
			if(tmp.length > 6) {
                let stream = beatmap.streams.get(old);
                stream?.push(tmp);
            }
			else
				beatmap.bursts.get(old)?.push(tmp);
		}
	}
// Log unique intervals
//	for(auto p : uniq)
//		cout << p << " ";

	// Log sections of 1/2 1/4 e.t.c.
//	i = 0;
//	int all = 0;
//	for(auto section : sections)
//	{
//		cout << "Section of " << msToBPM(section.first) << "bpm (" << section.first << "ms) groups:" << endl;
//		cout << endl;
//		int groups = 0, inGroup = 0, total = 0;
//		for(auto j : section.second)
//		{
//			inGroup = 0;
//			for(auto k : j)
//			{
//				cout << k << " ";
//				inGroup++;
//				total++;
//			}
//			cout << "Group by " << inGroup+1 << endl;
//			groups++;
//		}
//		cout << groups << " groups with " << total << " objects total" << endl;
//		all+=total;
//		cout << endl;
//		i++;
//	}
//
//	cout << "Objects in sections: " << all << ". In hitObjects: " << beatmap.pressIntervals.size() << endl;
//
//	// Log streams
//	i = 0;
//	for(auto stream : beatmap.streams)
//	{
//		cout << "Streams at " << msToBPM(stream.first) << "bpm (" << stream.first << "ms):" << endl;
//		cout << endl;
//		int groups = 0, inGroup = 0, total = 0;
//		for(auto j : stream.second)
//		{
//			inGroup = 0;
//			for(auto k : j)
//			{
//				cout << k << " ";
//				inGroup++;
//				total++;
//			}
//			cout << "has " << inGroup+1 << " objects" << endl;
//			groups++;
//		}
//		cout << groups << " streams with " << total << " objects total" << endl;
//		all+=total;
//		cout << endl;
//		i++;
//	}
//
//	// Log bursts
//	i = 0;
//	for(auto burst : beatmap.bursts)
//	{
//		cout << "Bursts at " << msToBPM(burst.first) << "bpm (" << burst.first << "ms):" << endl;
//		cout << endl;
//		int groups = 0, inGroup = 0, total = 0;
//		for(auto j : burst.second)
//		{
//			inGroup = 0;
//			for(auto k : j)
//			{
//				cout << k << " ";
//				inGroup++;
//				total++;
//			}
//			cout << "has " << inGroup+1 << " objects" << endl;
//			groups++;
//		}
//		cout << groups << " bursts with " << total << " objects total" << endl;
//		all+=total;
//		cout << endl;
//		i++;
//	}
}
