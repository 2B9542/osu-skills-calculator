import { IBeatmap, ITiming, Timing } from "../beatmap";
import { ISlider } from "../hit-objects/slider";
import { int } from "../utils";

export function gatherTargetPoints(beatmap: IBeatmap): void {
	let i: int = 0;
	let prev_time: int = Number.MIN_SAFE_INTEGER;

    beatmap.hitObjects.forEach(hitObj => {
        //  HACK: around objects that need to be hit impossibly fast
        //  filter out hitobjects that occur less than 5ms after another
        if (Math.abs(hitObj.time - prev_time) < 5) return;

		prev_time = hitObj.time;

        if(hitObj.isCircle()) {
            let targetPoint: ITiming = new Timing();
            targetPoint.time = hitObj.time;
			targetPoint.pos = hitObj.position;
			targetPoint.key = i;
			targetPoint.press = false;

			beatmap.targetPoints.push(targetPoint);
        }
        else if(hitObj.isSlider()) {
            let slider = hitObj as ISlider;
            slider.ticks.forEach(tick => {
                let targetPoint: ITiming = new Timing();
                targetPoint.time = tick;
				targetPoint.pos = slider.getPositionAt(tick);
				targetPoint.key = i;
				targetPoint.press = true;

				beatmap.targetPoints.push(targetPoint);
            });
        }

        i++;
    });
}