import { IBeatmap } from "../beatmap";
import { int } from "../utils";

export function calculatePressIntervals(beatmap: IBeatmap): void {
    let previousTime: int = -1;

    beatmap.hitObjects.forEach(hitObj => {
        if(previousTime !== -1)
            beatmap.pressIntervals.push(hitObj.time - previousTime);
        previousTime = hitObj.time;
    });
}