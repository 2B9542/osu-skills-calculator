import { IBeatmap } from "../beatmap";
import { calculateAngles } from "./calculate-angles";
import { calculateMovementData } from "./calculate-movement-data";
import { gatherAimPoints } from "./gather-aim-points";
import { gatherTargetPoints } from "./gather-target-points";

export function prepareAimData(beatmap: IBeatmap): void {
	calculateMovementData(beatmap);
	gatherTargetPoints(beatmap);
	gatherAimPoints(beatmap);
	calculateAngles(beatmap);
}
