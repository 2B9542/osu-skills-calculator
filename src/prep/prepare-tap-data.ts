import { IBeatmap } from "../beatmap";
import { calculatePressIntervals } from "./calculate-press-intervals";
import { gatherTapPatterns } from "./gather-tap-patterns";

export function prepareTapData(beatmap: IBeatmap): void {
	calculatePressIntervals(beatmap);
	gatherTapPatterns(beatmap);
}
