import { CircumscribedCircle } from "../hit-objects/circumscribed-circle";
import { IThingWithSliders } from "./approximate-slider-points";

export function bakeSliderData(beatmap: IThingWithSliders): void {
    beatmap.sliders = beatmap.sliders.map(sliderZ => {
        switch(sliderZ.curveType) {
            case "B": {
                sliderZ.generateCurves(false);
                sliderZ.lerpPoints = sliderZ.curves;
                sliderZ.ncurve = sliderZ.ncurve;
                break;
            }
            case "P":
            {
                if (sliderZ.curves.length == 2)
                {
                    //  FIX: surely has an issue
                    let ccircle = new CircumscribedCircle(sliderZ);
                    if(ccircle.temporaryFallback) {
                        sliderZ.generateCurves(true);
                    } else {
                        sliderZ.lerpPoints = Array.from(ccircle.curves);
                        sliderZ.ncurve = ccircle.ncurve;
                    }
                }
                else
                {
                    sliderZ.generateCurves(false);
                    sliderZ.lerpPoints = sliderZ.curves;
                    sliderZ.ncurve = sliderZ.ncurve;
                }
                break;
            }
            case "L": case "C":
            {
                sliderZ.generateCurves(true);
                sliderZ.lerpPoints = sliderZ.curves;
                sliderZ.ncurve = sliderZ.ncurve;
                break;
            }
        }

        sliderZ.endPoint = (sliderZ.repeat % 2) ? sliderZ.lerpPoints.pop() : sliderZ.lerpPoints.shift();
        return sliderZ;
    });
}
