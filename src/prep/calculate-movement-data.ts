import { IBeatmap } from "../beatmap";
import HitObjectType from "../hit-object-type";
import { Vector2 } from "../hit-objects/vector2";
import { CS2px, double, int } from "../utils";

export function calculateMovementData(beatmap: IBeatmap): void {
	let previousPos: Vector2;
	let previousTime: int = -1;

    let len = beatmap.hitObjects.length;
	for (let i = 0; i < len; i++) {
        let hitObject = beatmap.hitObjects[i];
        let isCircleOrSlider = (hitObject.isType(HitObjectType.Normal) || hitObject.isType(HitObjectType.SLIDER));
		if (isCircleOrSlider && previousTime !== -1) {
			let distance: double = Vector2.distance(hitObject.position, previousPos);
			let radSubtract: double = 2 * CS2px(beatmap.cs);
			let interval: double = hitObject.time - previousTime;
			if(distance >= radSubtract)
				distance -= radSubtract;
			else
				distance /= 2;

            beatmap.distances.push(distance);
			let distX: double = hitObject.position.X - previousPos.X;
			beatmap.velocities.X.push(distX / interval);
			let distY: double = hitObject.position.Y - previousPos.Y;
			beatmap.velocities.Y.push(distY / interval);
        }

        //  NOTSURE: maybe [i - 1] for sliders?
		if(isCircleOrSlider) {
			previousPos = hitObject.position;
			previousTime = hitObject.time;
		}
	}
	
	// Calculate velocity changes
	let oldvelX: double = 0, oldvelY = 0;
	for (let i = 0; i < beatmap.velocities.X.length; i++)
	{
		let velX: double = beatmap.velocities.X[i];
		let velY: double = beatmap.velocities.Y[i];
		if (i)
		{
			beatmap.velocities.Xchange.push(velX - oldvelX);
			beatmap.velocities.Ychange.push(velY - oldvelY);
		}
		oldvelX = velX;
		oldvelY = velY;
	}
}