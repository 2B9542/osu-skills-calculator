import { IBeatmap } from "../beatmap";
import HitObjectType from "../hit-object-type";
import { GetLastTickTime, ISlider } from "../hit-objects/slider";
import { Vector2 } from "../hit-objects/vector2";
import { CS2px, int } from "../utils";

export function gatherAimPoints(beatmap: IBeatmap): void {
    beatmap.hitObjects.forEach(hitObj => {
        if(hitObj.isType(HitObjectType.Normal)) {
			beatmap.aimPoints.push({
                time: hitObj.time,
                pos: hitObj.position, 
                type: AimPointType.Circle,
            });
        }

        else if(hitObj.isType(HitObjectType.SLIDER)) {
            let slider = hitObj as ISlider;

            beatmap.aimPoints.push({
                time: slider.time,
                pos: slider.position,
                type: AimPointType.Slider
            });
            
            let endTime: int = GetLastTickTime(slider);
            try {
                let endPos: Vector2 = slider.getPositionAt(endTime);
    
                // Don't add an end aim point for a very short slider
                if (slider.ticks.length || Vector2.distance(slider.position, endPos) > 2 * CS2px(beatmap.cs))
                    beatmap.aimPoints.push({
                        time: endTime,
                        pos: endPos,
                        type: AimPointType.SliderEnd
                    });
            } catch(ex) {
                debugger;
            }
        }
    });
}

export enum AimPointType {
	None,
	Circle,
	Slider,
	SliderReverse,
	SliderEnd,	
}

export interface IAimPoint {
	time: number;
	pos: Vector2;
	type: AimPointType;
}

export class AimPoint implements IAimPoint {
    time: number = 0;
    pos: Vector2 = new Vector2();
    type: AimPointType = AimPointType.None;
}
