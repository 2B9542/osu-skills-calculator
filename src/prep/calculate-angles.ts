import { IBeatmap } from "../beatmap";
import { degToRad, double, getDirectionalAngle, int } from "../utils";

export function calculateAngles(beatmap: IBeatmap): void {
    let len = beatmap.aimPoints.length;
	for (let i = 0; i + 2 < len; i++)
	{
		let angle: double = getDirectionalAngle(
            beatmap.aimPoints[i].pos, 
            beatmap.aimPoints[i + 1].pos, 
            beatmap.aimPoints[i + 2].pos
        );
		beatmap.angles.push(angle);
	}

    let oldangle: double = beatmap.angles[0] - 2 * beatmap.angles[0];
    beatmap.angles.forEach(angle => {
        let bonus: double = 0;
		let absd: double = Math.abs(angle);
		if (Math.sign(angle) === Math.sign(oldangle))
		{
			if (absd < 90)
				bonus = Math.sin(degToRad(absd) * 0.784 + 0.339837);
			else
				bonus = Math.sin(degToRad(absd));
		}
		else // when signs change
		{
			if (absd < 90)
				bonus = Math.sin(degToRad(absd) * 0.536 + 0.72972);
			else
				bonus = Math.sin(degToRad(absd)) / 2;
        }

		beatmap.angleBonuses.push(bonus);
		oldangle = angle;
    });
}