import { ITimingPoint } from "../beatmap";
import { double } from "../utils";

export function prepareTimingPoints(beatmap: ITimedThing) {
    // Get min and maxbpm
    beatmap.bpmMin = 10000;
    beatmap.bpmMax = 0;

    let BPM: double = 0;
    let SliderMult: double = -100;
    let oldbeat: double = -100;

    beatmap.timingPoints.forEach(tp => {
        if(tp.inherited) {
            if(tp.beatInterval <= 0) {
                SliderMult = tp.beatInterval;
                oldbeat = tp.beatInterval;
            }
            else
                SliderMult = oldbeat;
        }
        else
        {
            SliderMult = -100;
            BPM = 60000 / tp.beatInterval;
            if (beatmap.bpmMin > BPM)
                beatmap.bpmMin = BPM;
            if (beatmap.bpmMax < BPM)
                beatmap.bpmMax = BPM;			
        }
        tp.bpm = BPM;
        tp.sm = SliderMult;
    });
}

/**
 * TODO: Find a better name
 */
export interface ITimedThing {
    bpmMax: double;
    bpmMin: double;
    timingPoints: ITimingPoint[];
}
