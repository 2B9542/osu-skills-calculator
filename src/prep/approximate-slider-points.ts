import { IBeatmap } from "../beatmap";
import { ISlider, Slider } from "../hit-objects/slider";
import { Vector2 } from "../hit-objects/vector2";
import { double, getValuePos, int, uint } from "../utils";
import { ITimedThing } from "./prepare-timing-points";

export function approximateSliderPoints(beatmap: IBeatmap): void {
    let timingPointOffsets: double[] = [];
    let beatLengths: double[] = [];
    let base: double = 0;

    beatmap.timingPoints.forEach(tp => {
        timingPointOffsets.push(tp.offset);
        if (!tp.inherited)
            base = tp.beatInterval;
        beatLengths.push(base);
    });

    beatmap.sliders = beatmap.sliders.map(slider => {
        let timingPointIndex: int = getValuePos(timingPointOffsets, slider.time, true);
        let a = (100.0 * beatmap.sm);
        let b = (-600.0 / beatmap.timingPoints[timingPointIndex].bpm);
        let c = (b * slider.pixelLength * beatmap.timingPoints[timingPointIndex].sm);
        slider.toRepeatTime = Math.floor(Math.round((c / a)));
        slider.endTime = slider.time + slider.toRepeatTime * slider.repeat;
        
        slider.processRepeatTimes();
        slider.processTickTimestamps(beatLengths[timingPointIndex], beatmap.st);

        // If the slider starts and ends in less than 100ms and has no ticks to allow a sliderbreak, then make it a short generic slider
        let duration = Math.abs(slider.endTime - slider.time);
        let durationLessThan100ms = duration < 100;
        let noTicks = slider.ticks.length === 0;

        if (!(durationLessThan100ms && noTicks))
            return slider;

        let tickInterval: int = Math.floor(beatLengths[timingPointIndex] / beatmap.st);
        let sliderNew: ISlider = new Slider();
        sliderNew.position = slider.position;
        sliderNew.type = slider.type;
        sliderNew.time = slider.time;
        sliderNew.endTime = slider.time + 101;
        sliderNew.toRepeatTime = slider.time + 101;
        sliderNew.repeat = 1;
        sliderNew.pixelLength = 100;
        sliderNew.curveType = "L";
        sliderNew.curves = [ 
            new Vector2(slider.position.X, slider.position.Y), 
            new Vector2(slider.position.X + tickInterval / beatmap.st, slider.position.Y + tickInterval / beatmap.st) 
        ];

        sliderNew.generateCurves(true);
        
        return sliderNew;
    });
}

export interface IThingWithSliders extends ITimedThing {
    sliders: ISlider[];
    sm: double;
    st: double;
}