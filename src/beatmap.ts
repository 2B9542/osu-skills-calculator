import { IHitObject } from "./hit-objects/base";
import { ISlider } from "./hit-objects/slider";
import { Vector2 } from "./hit-objects/vector2";
import { double, int } from "./utils";
import ModType from "./mod-type";
import Easy from "./mods/easy";
import HardRock from "./mods/hard-rock";
import HalfTime from "./mods/half-time";
import DoubleTime from "./mods/double-time";
import { ITimedThing } from "./prep/prepare-timing-points";
import { IAimPoint } from "./prep/gather-aim-points";
import HitObjectType from "./hit-object-type";

/**
 * corresponds to TIMING in C++ osu-skills code
 */
export interface ITiming {
    /**
     * The time this occurs at
     */
    time: number;
    /**
     * generic data associated with this timing
     */
    data: number;
    /**
     * generic integral data (can be used for which key pressed, timing state, etc)
     */
    key: number;
    /**
     * generic boolean data (can be used to determine slider or not, pressed key or not, etc)
     */
    press: boolean;
    /**
     * additional timing info for 2d coordinates
     */
    pos: Vector2;
}

export class Timing implements ITiming {
    time: number = 0;
    data: number = 0;
    key: number = 0;
    press: boolean = false;
    pos: Vector2 = new Vector2();
}

export interface IVelocity {
    X: double[];
    Y: double[];
    Xchange: double[];
    Ychange: double[];
}

export class Velocity implements IVelocity {
    X: double[] = [];
    Y: double[] = [];
    Xchange: double[] = [];
    Ychange: double[] = [];
}

export interface IBeatmap extends ITimedThing {
    //#region  Metadata
    version: string;
    creator: string;
    artist: string;
    title: string;
    //#endregion

    spinners: number;
    name: string;
    ar: number;
    cs: number;
    od: number;
    hp: number;

    /**
     * slider velocity
     */
    sm: number;
    /**
     * slider tick rate
     */
    st: number;

    aimPoints: IAimPoint[];
    targetPoints: ITiming[];
    timingPoints: ITimingPoint[];
    hitObjects: IHitObject[];

    //#region Aim
    velocities: IVelocity;
    distances: double[];
    aimStrains: double[];
    angleStrains: double[];
    //#endregion

    //#region Tapping
    pressIntervals: int[];
    tapStrains: double[];
    //#endregion

    mods: ModType[];

    // helper for using time as an index to hitObjects vector
    timeMapper: Map<Number, Number>;

    //#region Reading
    /**
     * used for reading
     */
    angles: double[];
    /**
     * used for reading
     */
    angleBonuses: double[];
    /**
     * used for reading
     */
    reactionTimes: double[];
    //#endregion

    //  tenacity-related
    streams: Map<number, Array<Array<number>>>;

    //  stamina-related
    bursts: Map<number, Array<Array<number>>>;

    //  calculated skill values
    skills: ISkills;

    //#region extra
    sliders: ISlider[];
    circles: IHitObject[];
    //#endregion

    applyMods(mods: ModType[]): void;
    hasMod(mod: ModType): boolean;
}

export interface ITimingPoint {
    meter: number;
    offset: number;
    sm: number;
    bpm: number;
    beatInterval: number;
    inherited: boolean;
}

export class TimingPoint implements ITimingPoint {
    meter: number = 0;
    offset: number = 0;
    sm: number = 0;
    bpm: number = 0;
    beatInterval: number = 0;
    inherited: boolean = false;
}

export class Beatmap implements IBeatmap {
    version: string;
    creator: string;
    artist: string;
    title: string;

    name: string;

    ar: number = 0;
    cs: number = 0;
    od: number = 0;
    hp: number = 0;
    sm: number = -1;
    st: number = -1;

    aimPoints: IAimPoint[] = [];
    targetPoints: ITiming[] = [];

    //#region ITimedThing implementation
    timingPoints: ITimingPoint[] = [];
    bpmMin: number = 0;
    bpmMax: number = 0;
    //#endregion

    public hitObjects: IHitObject[] = [];

    spinners: number = 0;

    //#region Aim
    velocities: IVelocity = new Velocity();
    distances: double[] = [];
    aimStrains: double[] = [];
    angleStrains: double[] = [];
    //#endregion

    //#region Tapping
    pressIntervals: any[] = [];
    tapStrains: number[] = [];
    //#endregion

    mods: ModType[] = [];
    streams: Map<number, number[][]> = new Map<number, number[][]>();
    bursts: Map<number, number[][]> = new Map<number, number[][]>();

    timeMapper: Map<number, number> = new Map<number, number>();

    //#region Reading
    angles: double[] = [];
    angleBonuses: double[] = [];
    reactionTimes: double[] = [];
    //#endregion

    //@ts-ignore
    skills: ISkills = {};

    //#region extra
    private _sliders: ISlider[];
    private _circles: IHitObject[];

    public get circles(): IHitObject[] {
        if (this._circles === undefined)
            this._circles = this.hitObjects.filter(
                (o) => o.isType(HitObjectType.Normal)
            );
        return this._circles;
    }

    public get sliders(): ISlider[] {
        if (this._sliders === undefined)
            this._sliders = this.hitObjects.filter(
                (o) => o.isType(HitObjectType.SLIDER)
            ) as ISlider[];
        return this._sliders;
    }

    public set sliders(value: ISlider[]) {
        this.hitObjects = [...this.circles, ...value].sort((a, b) => {
            if(a.time < b.time)
                return -1;
            else if(a.time > b.time)
                return 1;
            else return 0;
        });
    }
    //#endregion

    public applyMods(mods: ModType[]): void {
        let ez = mods.includes(ModType.EZ);
        let hr = mods.includes(ModType.HR);
        let ht = mods.includes(ModType.HT);
        let dt = mods.includes(ModType.DT);

        if (ez && hr) return;

        if (ht && dt) return;

        if (ez)
            // Note: EZ needs to be applied BEFORE DT or HT
            Easy.apply(this);
        if (hr) HardRock.apply(this);
        if (ht) HalfTime.apply(this);
        if (dt) DoubleTime.apply(this);

        this.mods = new Array(...mods);
    }

    hasMod(mod: ModType): boolean {
        return this.mods.includes(mod);
    }
}

export interface ISkills {
    tenacity: number;
    stamina: number;
    accuracy: number;
    precision: number;
    agility: number;
    reaction: number;
}
