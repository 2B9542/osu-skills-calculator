import { Vector2 } from "./hit-objects/vector2";
import { IBeatmap } from "./beatmap";
import HitObjectType from "./hit-object-type";
import ModType from "./mod-type";

export const ActualPI = 3.14159265358979323846;

export type Pair<TFirst, TSecond> = {
    first: TFirst,
    second: TSecond,
}

export function makePair<TFirst, TSecond>(
    first: TFirst,
    second: TSecond
): Pair<TFirst, TSecond>
{
    return {
        first, second,
    };
}

export type double = number;
export type int = number;
export type uint = number;
export type long = number;

export function max_element(numbers: number[]) {
    let max = Number.NEGATIVE_INFINITY;
    let len = numbers.length;
    while(len--)
        max = numbers[len] > max ? numbers[len] : max;

    return max;
}

export function getWeightedValue2(vals: number[], decay: number): number {
    let result = 0;
    let len = vals.length;
	for (let i = 0; i < len; i++)
		result += vals[i] * Math.pow(decay, i);
	return result;
}

export function getPeakVals(vals: number[], output: number[]) {
    let len = vals.length;
    for (let i = 0; i < len; i++) {
		if (vals[i] > vals[i - 1] && vals[i] > vals[i + 1])
            output.push(vals[i]);
    }

    output.sort().reverse();
}

export function OD2ms(od: number): number
{
	return -6.0 * od + 79.5;
}

export function IsHitObjectType(Type: number, type: HitObjectType): boolean
{
	return (Type & type) > 0;
}

export function logf(x: number) {
    return Math.log(x);
}

export function sqrtf(x: number) {
    return Math.sqrt(x);
}

export function inverseError(x: number): number // Inverse Error Function 
{
	let w: number, p: number;

	w = -logf((1.0 - x) * (1.0 + x));

	if (w < 5.000000)
	{
		w = w - 2.500000;
		p =  2.81022636e-08;
		p =  3.43273939e-07 + p*w;
		p = -3.5233877e-06  + p*w;
		p = -4.39150654e-06 + p*w;
		p =  0.00021858087  + p*w;
		p = -0.00125372503  + p*w;
		p = -0.00417768164  + p*w;
		p =  0.246640727    + p*w;
		p =  1.50140941     + p*w;
	}
	else 
	{
		w = sqrtf(w) - 3.000000;
		p = -0.000200214257;
		p =  0.000100950558 + p*w;
		p =  0.00134934322  + p*w;
		p = -0.00367342844  + p*w;
		p =  0.00573950773  + p*w;
		p = -0.0076224613   + p*w;
		p =  0.00943887047  + p*w;
		p =  1.00167406     + p*w;
		p =  2.83297682     + p*w;
	}
	return p*x;
}

export function getValuePos(list: number[], value: number, order: boolean): number
{
	// Until binary search is fixed, use this
	if (!order) // descending
	{
        let len = list.length - 1;
		for (let i = len; i >= 1; i--)
			if (list[i - 1] < value)
				return i;
		return 0;
	}
	else // ascending
	{
        let len = list.length - 1;
		for (let i = 0; i < len; i++)
			if (list[i + 1] > value)
				return i;
		return len;
	}
}

/**
 * converts AR value to ms visible before needed to click
 * @param ar 
 */
export function AR2ms(ar: double): int
{
	if (ar <= 5.00) return Math.floor(1800 - (120 * ar));
	else			return Math.floor(1950 - (150 * ar));
}

/**
 * converts ms visible before needed to click to AR value
 * @param ms 
 */
export function ms2AR(ms: int): double
{
	if (ms >= 1200.0) return (1800 - ms) / 120.0;
	else			  return (1950 - ms) / 150.0;
}


/**
 * converts CS value into circle radius in osu!pixels (640x480)
 * @param cs 
 */
export function CS2px(cs: number): number
{
	return Math.floor(54.5 - (4.5 * cs));
}

export function degToRad(degrees: number): number {
    return (degrees * ActualPI / 180.0);
}

export function rad2Deg(radians: number): number {
    return (radians * 180.0) / ActualPI;
}

/**
 * Returns the angle 3 points make in radians between 0 and pi
 * @param a 
 * @param b 
 * @param c 
 */
export function GetAngle(
    a: Vector2, 
    b: Vector2,
    c: Vector2
): number
{
	return Math.abs(degToRad(getDirectionalAngle(a, b, c)));
}

/**
 * Gets the directional angle in degrees (-180 -> +180)
 * Positive is counter-clock wise and negative is clock-wise
 * @param a 
 * @param b 
 * @param c 
 */
export function getDirectionalAngle(
    a: Vector2,
    b: Vector2,
    c: Vector2
): number
{
	let ab: Vector2 = new Vector2(b.X - a.X, b.Y - a.Y);
	let cb: Vector2 = new Vector2(b.X - c.X, b.Y - c.Y);

	let dot: number = (ab.X * cb.X + ab.Y * cb.Y); // dot product
	let cross: number = (ab.X * cb.Y - ab.Y * cb.X); // cross product

	let alpha: number = Math.atan2(cross, dot);

	return alpha * 180.0 / Math.PI;
}


/**
 * resolves % value between from min to max
 * ex: 50% of something from 100 to 200 is 150
 * (isn't this linear interpolation?)
 * @param min 
 * @param max 
 * @param percent 
 */
export function getValue(
    min: number,
    max: number,
    percent: number
): number
{
    let _max = Math.max(max, min);
	return _max - (1.0 - percent) * (_max - Math.min(max, min));
}

/**
 * Is the value in the [ less, greater ] range?
 * @param less left margin
 * @param value value
 * @param greater right margin
 */
export function BTWN(
    less: number,
    value: number,
    greater: number
): boolean
{
	return ((Math.min(less, greater) <= value) && (value <= Math.max(less, greater)));
}

/**
 * Clamps value between min and max
 * @param value 
 * @param min 
 * @param max 
 */
export function clamp(
    value: number,
    min: number,
    max: number,
) {
    return value < min ? min : value > max ? max : value;
}

export function lerp(
    source: double,
    destination: double,
    delta: double
): double {
    return source + delta * (destination - source);
}

export function clear<T>(arr: Array<T>) {
    let len = arr.length;
    while(len--)
        arr.pop();
}

/**
* Calculates the Bernstein polynomial.
* @param i the index
* @param n the degree of the polynomial (i.e. number of points)
* @param t the t value [0, 1]
*/
export function bernstein(i: int, n: int, t: double): double {
	return binomialCoefficient(n, i) * Math.pow(t, i) * Math.pow(1 - t, n - i);
}

/**
 * Calculates the binomial coefficient.
 * http://en.wikipedia.org/wiki/Binomial_coefficient#Binomial_coefficient_in_programming_languages
 */
export function binomialCoefficient(n: int, k: int): long {
    if (k < 0 || k > n)		return 0;
	if (k == 0 || k == n)	return 1;

	k = Math.floor(Math.min(k, n - k));  // take advantage of symmetry
	let c: long = 1;
	for (let i: int = 0; i < k; i++)
		c = c * (n - i) / (i + 1);

	return c;
}
