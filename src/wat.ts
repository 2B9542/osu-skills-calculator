import * as path from "path";
import { CalculateBeatmapSkills, } from "./index";

const its = 1;
const name = "v14";
let skills = null;

console.time(name);
for (let i = 0; i < its; i++) {
    let p = path.join(__dirname, "../maps", `${name}.osu`);

    skills = CalculateBeatmapSkills(p, 0);
}
console.timeEnd(name);
console.log(skills);

function calculateFor(name: string) {
    let p = path.join(__dirname, "../maps", `${name}.osu`);
    console.time(name);
    let skills = CalculateBeatmapSkills(p, 0);
    console.timeEnd(name);
    console.log(name, skills);
    return skills;
}
