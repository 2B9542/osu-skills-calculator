import * as fs from "fs";
import { HitObject, IHitObject } from "./hit-objects/base";
import { Slider } from "./hit-objects/slider";
import { Vector2 } from "./hit-objects/vector2";

import { Beatmap, IBeatmap, ITimingPoint, TimingPoint, } from "./beatmap";
import HitObjectType from "./hit-object-type";
import { boolean } from "mathjs";

export function tokenize(
    str: string, 
    delimiter: string = " ",
    trimEmpty: boolean = false
): ITokenizeResult {
    return {
        tokens: trimEmpty ? str.split(delimiter).map(s => s.trim()) : str.split(delimiter),
    };
}

export function ParseBeatmap(filepath: string): IBeatmap {
    let content = fs.readFileSync(filepath).toString();

    return new FormatV7().parse(content);
}

export function GetMapListFromFile(filepath: string): string[] {
    let mapList: string[] = [];

    let listFile = fs.readFileSync(filepath).toString();
    let lines = listFile.split("\n");
    lines.forEach(line => {
        if(line.length === 0) return;
        // ignore commented maps
        if(line.indexOf("//") === -1) return;

        mapList.push(line);
    });

    return mapList;
}

interface IBeatmapParser {
    parse(text: string): IBeatmap;
}

enum Found {
    Nothing,
    Metadata,
    Difficulty,
    TimingPoints,
    HitObjects,
}

class FormatV7 implements IBeatmapParser {
    parse(text: string): IBeatmap {
        let beatmap: IBeatmap = new Beatmap();
        let lines = text.split("\n");
        let found = Found.Nothing;

        lines.forEach(line => {
            line = line.replace(/(\r\n|\n|\r)/gm,"");

            if(line.length === 0) return;

            switch(line) {
                case "[Metadata]": {
                    return found = Found.Metadata;
                }
                case "[Difficulty]": {
                    return found = Found.Difficulty;
                }
                case "[TimingPoints]": {
                    return found = Found.TimingPoints;
                }
                case "[HitObjects]": {
                    return found = Found.HitObjects;
                }
                default: {
                    switch(found) {
                        case Found.Metadata: {
                            let { tokens } = tokenize(line, ":");

                            switch(tokens[0]) {
                                case "Title": {
                                    return beatmap.title = tokens[1];
                                }
                                case "Artist": {
                                    return beatmap.artist = tokens[1];
                                }
                                case "Creator": {
                                    return beatmap.creator = tokens[1];
                                }
                                case "Version": {
                                    return beatmap.version = tokens[1];
                                }
                                default: {
                                    return;
                                }
                            }
                        }

                        case Found.Difficulty: {
                            let { tokens } = tokenize(line, ":");

                            switch(tokens[0]) {
                                case "ApproachRate": {
                                    return beatmap.ar = Number.parseFloat(tokens[1]);
                                }
                                case "HPDrainRate": {
                                    return beatmap.hp = Number.parseFloat(tokens[1]);
                                }
                                case "CircleSize": {
                                    return beatmap.cs = Number.parseFloat(tokens[1]);
                                }
                                case "OverallDifficulty": {
                                    return beatmap.od = Number.parseFloat(tokens[1]);
                                }
                                case "SliderMultiplier": {
                                    return beatmap.sm = Number.parseFloat(tokens[1]);
                                }
                                case "SliderTickRate": {
                                    return beatmap.st = Number.parseFloat(tokens[1]);
                                }
                                default: {
                                    return;
                                }
                            }
                        }

                        case Found.TimingPoints: {
                            let { tokens } = tokenize(line, ",");

                            //  FIX: what the fuck
                            if(tokens.length < 2)
                                return found = Found.Nothing;

                            let point: ITimingPoint = new TimingPoint();
                            point.offset = Number.parseInt(tokens[0]);
                            point.beatInterval = Number.parseInt(tokens[1]);
                            point.inherited = tokens.length > 6 && !Number.parseInt(tokens[6]);
                            
                            if(tokens.length >= 3)
                                point.meter = Number.parseInt(tokens[2]);
                            else
                                point.meter = 4;    //  old maps

                            beatmap.timingPoints.push(point);
                            return;
                        }

                        case Found.HitObjects: {
                            let tokens = line.split(",");
                            parseHitObjectsV2(tokens, beatmap);
                            return;
                        }
                    }
                }
            }
        });

        if (beatmap.ar === 0)
            beatmap.ar = beatmap.od;

        beatmap.name = `${beatmap.artist} - ${beatmap.title} (${beatmap.creator}) [${beatmap.version}]`;

        return beatmap;
    }
}

function parseHitObjectsV2(tokens: string[], beatmap: IBeatmap) {
    function isType(actual: number, expected: HitObjectType) {
        return boolean(actual & expected) as boolean;
    }

    let type: number = Number.parseInt(tokens[3]);
    
    if(isType(type, HitObjectType.Spinner)) {
        // let spinner = new Spinner();
        // spinner.pos.X = Number.parseFloat(tokens[0]);
        // spinner.pos.Y = Number.parseFloat(tokens[1]);
        // spinner.type  = type;
        // spinner.time  = Number.parseInt(tokens[2]);
        // spinner.endTime = spinner.time;
        beatmap.spinners++;
        return;
    }

    else if(isType(type, HitObjectType.ManiaLong) || isType(type, HitObjectType.Hold)) {
        // hitObject = new HitObject();
        // hitObject.pos.X = Number.parseFloat(tokens[0]);
        // hitObject.pos.Y = Number.parseFloat(tokens[1]);
        // hitObject.type  = type;
        // hitObject.time  = Number.parseInt(tokens[2]);
        // hitObject.endTime = hitObject.time;
        return;
    }

    else if(isType(type, HitObjectType.SliderNewCombo) || isType(type, HitObjectType.SLIDER)) {
        let slider = new Slider();
        slider.position.X = Number.parseFloat(tokens[0]);
        slider.position.Y = Number.parseFloat(tokens[1]);
        slider.type  = type;
        slider.time  = Number.parseInt(tokens[2]);
        slider.endTime = slider.time;
        slider.pixelLength = 0;
        slider.repeat = 1;
        slider.ncurve = 0;
        slider.toRepeatTime = 0;

        let { tokens: sliderTokens } = tokenize(tokens[5], "|");
        slider.curveType = sliderTokens[0][0];
        
        for (let i = 1; i < sliderTokens.length; i++)
        {
            let { tokens: curveTokens } = tokenize(sliderTokens[i], ":");
            
            let curve: Vector2 = new Vector2(
                Number.parseInt(curveTokens[0]),
                Number.parseInt(curveTokens[1])
            );
            
            slider.curves.push(curve);
        }

        slider.repeat = Number.parseInt(tokens[6]);
        slider.pixelLength = Number.parseFloat(tokens[7]);
        beatmap.hitObjects.push(slider);

        beatmap.timeMapper[slider.time] = beatmap.hitObjects.length - 1;
        return;
    }

    else if(isType(type, HitObjectType.NormalNewCombo) || isType(type, HitObjectType.Normal)) {
        let circle = new HitObject();
        circle.position.X = Number.parseFloat(tokens[0]);
        circle.position.Y = Number.parseFloat(tokens[1]);
        circle.type  = type;
        circle.time  = Number.parseInt(tokens[2]);
        // hitObject.endTime = hitObject.time;
        // hitObject.endPoint = hitObject.pos;
        beatmap.hitObjects.push(circle);
        beatmap.timeMapper[circle.time] = beatmap.hitObjects.length - 1;
        return;
    }
}

interface ITokenizeResult {
    tokens: string[],
}