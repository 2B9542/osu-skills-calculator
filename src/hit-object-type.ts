enum HitObjectType {
    Normal = 1,
	SLIDER = 2,
	NewCombo = 4,
	NormalNewCombo = 5,
	SliderNewCombo = 6,
	Spinner = 8,
	ColourHax = 112,
	Hold = 128,
	ManiaLong = 128
}

export default HitObjectType;